// [Section] JavaScript Synchronous vs Asynchronous

	// Synchronous
	/*
		- JavaScript is by default synchronous
		- Only one statement can be executed at a time
		- Code blocking
	*/

		console.log("Hello World");
		/*conosole.log("Hello Again");*/
		console.log("Goodbye");

	// Asynchronous
		// CHECK the status of the request
		/*
			- We can proceed to execute other statements, while time consuming codes are running in the background.
			
			- fetch() - Fetch API allows to asynchronously request for a resource (data)
			- Syntax of fetch()
				fetch('URL')

			- A "promise" is an object that represents the eventual completion of an asynchronous function and its resulting value

			- By using the .then() method, we can now check the status of "promise"
			- Syntax
				.then((response) => {})

			- The fetch() method will return a "promise" that resolves to a "response" object
			- The .then() method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"
		*/

			fetch('https://jsonplaceholder.typicode.com/posts')
			.then(response => console.log(response.status));


		// RETRIEVE contents/data from the response object using GET Method
		/*
			- Using multiple .then() method, creates a "promise chain"
		*/

			fetch('https://jsonplaceholder.typicode.com/posts')
			.then((response) => response.json())
			.then((json) => console.log(json))


			// Creating a function that will demonstrate using "async" and "await" keywords
			/*
				- The "async" and "await" keywords are another approach that can be used to achieve an asynchronous code.
				- Used in functions to indicate which portions of code should be waited for
				- It creates an asynchronous function

				- It waits for the fetch() method to complete, then stores the value in the result variable
				- console.log(result); - The result that is returned by fatch is a "promise"
				- console.log(typeof result); - object
				- console.log(result.body); - We cannot access the content of the "response" directly by accessing its body property
				- let json = await result.json(); - converts the data from the "response" object as JSON
			*/

				async function fetchData(){
					let result = await fetch('https://jsonplaceholder.typicode.com/posts')
					console.log(result);
					console.log(typeof result);
					console.log(result.body);

					let json = await result.json();
					console.log(json);
				}

				fetchData();


		// Retrieve a Specific Post (added "1" to find the user id 1)
		
			fetch('https://jsonplaceholder.typicode.com/posts/1')
			.then((response) => response.json())
			.then((json) => console.log(json))


		// CREATE a post using POST Method
		/*
			- method - sets the method of the "request" object to post
			- headers - specified that the content will be in a json structure
			- Syntax:
				fetch("URL", options)
				.then((response) => {})
				.then((response) => {})
		*/

			fetch('https://jsonplaceholder.typicode.com/posts',{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},

				body: JSON.stringify({
					title: "New Post",
					body: "Hello World!",
					userId: 1
				})
			})
			.then((response) => response.json())
			.then((json) => console.log(json))


		// UPDATE a post using PUT Method

			fetch('https://jsonplaceholder.typicode.com/posts', {
				method: "PUT",
				headers: {
					'Content-Type':'application/json'
				},

				body: JSON.stringify({
					id: 1,
					title: "Updated post",
					body: "Hello again!",
					userId: 1
				})
			})
			.then((response) => response.json())
			.then((json) => console.log(json))


		// UPDATE a post using PATCH Method
		/*
			- Patch is used to update the whole object
			- Put is used to update a single property
		*/

			fetch('https://jsonplaceholder.typicode.com/posts/1',{
				method: "PATCH",
				headers: {
					'Content-Type': 'application/json'
				},

				body: JSON.stringify({
					title: "Corrected post"
				})
			})

			.then((response) => response.json())
			.then((json) => console.log(json));


		// DELETE a post

			fetch('https://jsonplaceholder.typicode.com/posts/1',{
				method: "DELETE"
			})